helm install treso --dry-run --debug  --namespace test-prod .

Pensez à mettre `dolibarr-asso` comme nom de release et à bien changer le nom de la base de données (fullNameOverride) `mariadb-dolibarr-asso`.

# Installation after
You can do the installation after via the WebUI but in case there is a timeout because the installation can take a long time, you can do manual curl in the dolibarr container. Make sure to change the values

```
  curl -L -X POST http://localhost:8080/install/check.php \
    --data-urlencode "testpost=ok" \
    --data-urlencode "action=set" \
    --data-urlencode "selectlang=fr_FR"

  curl -L -X POST http://localhost:8080/install/step1.php \
    --data-urlencode "testpost=ok" \
    --data-urlencode "action=set" \
    --data-urlencode "selectlang=fr_FR" \
    --data-urlencode "main_dir=/var/www/html/htdocs" \
    --data-urlencode "main_data_dir=/var/www/html/documents" \
    --data-urlencode "main_url=https://treso.domain.fr" \
    --data-urlencode "db_name=change_me" \
    --data-urlencode "db_host=treso-mariadb" \
    --data-urlencode "db_user=change_me" \
    --data-urlencode "db_pass=change_me" \
    --data-urlencode "db_prefix=llx_" \
    --data-urlencode "db_port=3306" \
    --data-urlencode "db_type=mysqli"


  curl -L -X POST http://localhost:8080/install/step2.php \
    --data-urlencode "testpost=ok" \
    --data-urlencode "action=set" \
    --data-urlencode "selectlang=fr_FR" \
    --data-urlencode "dolibarr_main_db_character_set=utf8" \
    --data-urlencode "dolibarr_main_db_collation=utf8_unicode_ci" 
  
  curl -L -X POST http://localhost:8080/install/step4.php \
    --data-urlencode "testpost=ok" \
    --data-urlencode "action=set" \
    --data-urlencode "selectlang=fr_FR" \
    --data-urlencode "dolibarrpingno=checked"

  curl -L -X POST http://localhost:8080/install/step5.php \
    --data-urlencode "testpost=ok" \
    --data-urlencode "action=set" \
    --data-urlencode "selectlang=fr_FR" \
    --data-urlencode "login=admin" \
    --data-urlencode "pass=change_me" \
    --data-urlencode "pass_verif=change_me"
```
